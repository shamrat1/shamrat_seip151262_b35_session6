<?php
//a php program determine the largest variable from 3 variable using if else

$v1=4;
$v2=10;
$v3=1;

if($v1>$v2 && $v1>$v3){
    echo 'variable 1 has the largest value. Value:'.$v1.'<br>';
}
elseif($v2>$v1 && $v2>$v3){
    echo 'variable 2 has the largest value. Value:'.$v2.'<br>';
}
else{
    echo 'variable 3 has the largest value. Value:'.$v3.'<br>';
}
?>
<br>
<?php
//a php program to print the prime number;

$number=2;
while($number<=100){
    $count=0;
    for($i=1;$i<$number;$i++){
        if($number%$i==0){
            $count++;
        }
    }
    if($count<3){
        echo $number.',';
    }
    $number++;
}
?>
<br>
<?php
$number = 5;                   /*number to get factorial */
$fact   = 1;
for($k=1;$k<=$number;++$k)
{
    $fact =  $fact*$k;
}
echo "Factorial of $number is ".$fact;